# -*- encoding: utf-8 -*-

from pytest import mark
from ui_test.data import Data
from pages.login_page import LoginPage
from ui_test.base_test import BaseTest


@mark.login
class TestSuccessfulLogin(BaseTest):

    def check_successfull_login(self):
        login = LoginPage(self.driver)
        login.insert_credentials(Data.EMAIL, Data.PASS)
        home = login.click_login()
        home.verify_user_menu_title()
