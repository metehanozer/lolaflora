# -*- encoding: utf-8 -*-


class Data:

    LOGIN_URL = 'https://www.mizu.com/en-es/login'

    EMAIL = 'testi.yapan@outlook.com.tr'
    PASS = 'Testi1234!'
    SAMPLE_EMAIL = 'sample@mail.com'
    SAMPLE_PASS = 'sample'

    EMPTY_INPUT_MESSAGE = 'Required field.'
    EMAIL_FORMAT_MESSAGE = 'Please enter a valid e-mail address.'
    PASSWORD_FORMAT_MESSAGE = 'Please enter minimum 3 and maximum 20 characters.'
    UNSUCCESSFUL_LOGIN_MESSAGE = 'E-mail address or password is incorrect. Please check your information and try again.'
    REGISTERED_FORGOT_PASSWORD_MESSAGE = 'You will receive an e-mail from us with instructions for resetting your password.'
    UNREGISTERED_FORGOT_PASSWORD_MESSAGE = 'Email address is not registered the system.'

    EMAIL_ERROR_LIST = [
        ('', EMPTY_INPUT_MESSAGE),
        ('not_email', EMAIL_FORMAT_MESSAGE),
        ('not_email@', EMAIL_FORMAT_MESSAGE),
        ('@not_email.com', EMAIL_FORMAT_MESSAGE)
    ]

    PASSWORD_ERROR_LIST = [
        ('', EMPTY_INPUT_MESSAGE),
        ('1', PASSWORD_FORMAT_MESSAGE),
        ('123456789012345678901', PASSWORD_FORMAT_MESSAGE)
    ]

    INVALID_CREDENTIALS_LIST = [
        (EMAIL, 'wrong_pass'),
        (EMAIL, 'True'),
        (EMAIL, 'true'),
        (EMAIL, '1=1'),
        (EMAIL, '1==1'),
        (EMAIL, 'if 1 == 1:'),
        (EMAIL, 'if (1 == 1)'),
        (EMAIL, 'admin'),
        (SAMPLE_EMAIL, PASS)
    ]
