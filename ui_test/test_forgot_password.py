# -*- encoding: utf-8 -*-

from pytest import mark
from ui_test.data import Data
from pages.login_page import LoginPage
from ui_test.base_test import BaseTest


@mark.login
class TestForgotPassword(BaseTest):

    def check_password_recovery_input(self):
        page = LoginPage(self.driver)
        page.click_password_recovery()
        page.verify_password_recovery_input_is_visible()

    def check_password_recovery_button(self):
        page = LoginPage(self.driver)
        page.click_password_recovery()
        page.verify_password_recovery_button_is_visible()

    @mark.parametrize('email, expected_message', Data.EMAIL_ERROR_LIST)
    def check_password_recovery_error(self, email, expected_message):
        page = LoginPage(self.driver)
        page.click_password_recovery()
        page.insert_password_recovery_email(email)
        page.click_password_recovery_send_button()
        page.verify_password_recovery_error(expected_message)

    def check_password_recovery_message_with_unregistered_email(self):
        page = LoginPage(self.driver)
        page.click_password_recovery()
        page.insert_password_recovery_email(Data.SAMPLE_EMAIL)
        page.click_password_recovery_send_button()
        page.verify_password_recovery_unregistered_email_message()

    def check_password_recovery_message_with_registered_email(self):
        page = LoginPage(self.driver)
        page.click_password_recovery()
        page.insert_password_recovery_email(Data.EMAIL)
        page.click_password_recovery_send_button()
        page.verify_password_recovery_registered_email_message()
