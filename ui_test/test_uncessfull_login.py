# -*- encoding: utf-8 -*-

from pytest import mark
from ui_test.data import Data
from pages.login_page import LoginPage
from ui_test.base_test import BaseTest


@mark.login
class TestUnsuccessfulLogin(BaseTest):

    @mark.parametrize('email, expected_message', Data.EMAIL_ERROR_LIST)
    def check_email_input_error(self, email, expected_message):
        page = LoginPage(self.driver)
        page.insert_credentials(email, Data.SAMPLE_PASS)
        page.click_login()
        page.verify_email_error_label(expected_message)

    @mark.parametrize('password, expected_message', Data.PASSWORD_ERROR_LIST)
    def check_password_input_error(self, password, expected_message):
        page = LoginPage(self.driver)
        page.insert_credentials(Data.SAMPLE_EMAIL, password)
        page.click_login()
        page.verify_password_error_label(expected_message)

    def check_password_show_function(self):
        page = LoginPage(self.driver)
        page.insert_credentials(Data.EMAIL, Data.PASS)
        page.click_password_show_button()
        page.verify_password_show_function()

    @mark.parametrize('email, password', Data.INVALID_CREDENTIALS_LIST)
    def check_unsuccessfull_login(self, email, password):
        page = LoginPage(self.driver)
        page.insert_credentials(email, password)
        page.click_login()
        page.verify_uncessfull_login_message()
