# -*- encoding: utf-8 -*-

from time import sleep
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains


def sleep_while():
    sleep(0.05)


class BasePage:
    WAIT_TIME = 30

    def __init__(self, driver):
        self.driver = driver

    def click(self, by):
        Wait(self.driver, self.WAIT_TIME).until(ec.visibility_of_element_located(by)).click()
        sleep_while()

    def hover(self, by):
        element = Wait(self.driver, self.WAIT_TIME).until(ec.visibility_of_element_located(by))
        ActionChains(self.driver).move_to_element(element).perform()
        sleep_while()

    def send_keys(self, by, text):
        Wait(self.driver, self.WAIT_TIME).until(ec.visibility_of_element_located(by)).send_keys(text)
        sleep_while()

    def get_text(self, by):
        return Wait(self.driver, self.WAIT_TIME).until(ec.visibility_of_element_located(by)).text

    def get_attribute(self, by, attribute):
        return Wait(self.driver, self.WAIT_TIME).until(ec.visibility_of_element_located(by)).get_attribute(attribute)

    def is_visible(self, by):
        element = Wait(self.driver, self.WAIT_TIME).until(ec.visibility_of_element_located(by))
        return bool(element)

    def assert_element_is_visible(self, by):
        assert self.is_visible(by) is True

    def assert_text(self, by, title, expected_text):
        acrual_text = self.get_text(by)
        assert acrual_text == expected_text, \
            f'{title} is displayed incorrectly!\n' \
            f'Actual text: {acrual_text}\n' \
            f'Expected text: {expected_text}'
