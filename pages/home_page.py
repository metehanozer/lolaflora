# -*- encoding: utf-8 -*-

from selenium.webdriver.common.by import By
from pages.base_page import BasePage


class HomePage(BasePage):

    USER_MENU_TITLE = (By.CSS_SELECTOR, '.header__right-col .user-menu__item--account .user-menu__title')

    def __init__(self, driver):
        super().__init__(driver)

    def verify_user_menu_title(self):
        self.assert_text(self.USER_MENU_TITLE, 'Logged user menu title', 'My Account')
