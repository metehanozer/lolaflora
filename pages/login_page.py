# -*- encoding: utf-8 -*-

from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from pages.home_page import HomePage
from ui_test.data import Data


class LoginPage(BasePage):

    URL = 'https://www.mizu.com/en-es/login'

    EMAIL_INPUT = (By.ID, 'EmailLogin')
    EMAIL_ERROR_LABEL = (By.ID, 'EmailLogin-error')
    PASSWORD_INPUT = (By.ID, 'Password')
    PASSWORD_SHOW_BUTTON = (By.CSS_SELECTOR, '#userLogin .form-group__show-type')
    PASSWORD_ERROR_LABEL = (By.ID, 'Password-error')
    LOGIN_BUTTON = (By.CSS_SELECTOR, '.js-login-button')
    UNSUCCESSFUL_LOGIN_MESSAGE = (By.CSS_SELECTOR, '#modalBox .modal-body')
    PASSWORD_RECOVERY_LINK = (By.CSS_SELECTOR, '.login__forgot-password')
    PASSWORD_RECOVERY_INPUT = (By.ID, 'Mail')
    PASSWORD_RECOVERY_BUTTON = (By.XPATH, '//button[contains(.,"Send")]')
    PASSWORD_RECOVERY_ERROR = (By.ID, 'Mail-error')
    PASSWORD_RECOVERY_SUCCESS = (By.CSS_SELECTOR, '.password-recovery-result')

    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(self.URL)

    def insert_credentials(self, email, password):
        self.send_keys(self.EMAIL_INPUT, email)
        self.send_keys(self.PASSWORD_INPUT, password)

    def click_password_show_button(self):
        self.click(self.PASSWORD_SHOW_BUTTON)

    def verify_password_show_function(self):
        actual_text = self.get_attribute(self.PASSWORD_INPUT, 'type')
        assert actual_text == 'text'

    def click_login(self):
        self.click(self.LOGIN_BUTTON)
        return HomePage(self.driver)

    def verify_email_error_label(self, expected_text):
        self.assert_text(self.EMAIL_ERROR_LABEL, 'Email input label text', expected_text)

    def verify_password_error_label(self, expected_text):
        self.assert_text(self.PASSWORD_ERROR_LABEL, 'Email input label text', expected_text)

    def verify_uncessfull_login_message(self):
        actual_text = self.get_text(self.UNSUCCESSFUL_LOGIN_MESSAGE)
        assert actual_text == Data.UNSUCCESSFUL_LOGIN_MESSAGE

    def click_password_recovery(self):
        self.click(self.PASSWORD_RECOVERY_LINK)

    def verify_password_recovery_input_is_visible(self):
        self.assert_element_is_visible(self.PASSWORD_RECOVERY_INPUT)

    def verify_password_recovery_button_is_visible(self):
        self.assert_element_is_visible(self.PASSWORD_RECOVERY_BUTTON)

    def insert_password_recovery_email(self, email):
        self.send_keys(self.PASSWORD_RECOVERY_INPUT, email)

    def click_password_recovery_send_button(self):
        self.click(self.PASSWORD_RECOVERY_BUTTON)

    def verify_password_recovery_error(self, expected_text):
        self.assert_text(self.PASSWORD_RECOVERY_ERROR, 'Password recovery email input label text', expected_text)

    def verify_password_recovery_unregistered_email_message(self):
        self.assert_text(self.PASSWORD_RECOVERY_SUCCESS,
                         'Password recovery message with unregistered email address',
                         Data.UNREGISTERED_FORGOT_PASSWORD_MESSAGE)

    def verify_password_recovery_registered_email_message(self):
        self.assert_text(self.PASSWORD_RECOVERY_SUCCESS,
                         'Password recovery message with registered email address',
                         Data.REGISTERED_FORGOT_PASSWORD_MESSAGE)
