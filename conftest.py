# -*- encoding: utf-8 -*-

import chromedriver_autoinstaller
from pytest import fixture, mark
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options


@fixture(scope='class')
def init_chrome_driver(request):
    chrome_options = Options()
    chrome_options.add_argument('--log-level=3')
    chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])

    chromedriver_autoinstaller.install()
    chrome_driver = Chrome(options=chrome_options)
    chrome_driver.implicitly_wait(30)
    chrome_driver.maximize_window()
    request.cls.driver = chrome_driver
    yield chrome_driver
    chrome_driver.quit()
