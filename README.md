# lolaflora

Proje python 3.9 ile yazılmıştır. Aşağıdaki komut ile bağımlılıklar yüklenebilir.
```
pip install -r requirements.txt
```

Test çerçevesi olarak gelişmiş fixture ve parametrize özelliklerinden ötürü PyTest kullanılmıştır.

UI testleri selenium webdriver kütüphanesi kullanılarak yazılmıştır. UI testlerinde projenin bakımını kolaylaştırmak ve sade bir yapı izlemek için Page Object Model (POM) kullanılmıştır. UI testleri chrome driver ile koşmaktadır. chrome driver chromedriver-autoinstaller kütüphanesi ile otomatik olarak indirilmektedir.

Load test için kullanım kolaylığından ve grafiksel sonuçlardan dolayı python locust kütüphanesi kullanılmıştır. Bunun dışında Apache Jmeter testi de eklenmiştir.

Api testleri ise requests kütüphanesi ile yazılmıştır.

### Senaryo 1:
```
python -m pytest -vv -m login
```

### Senaryo 2:
Aşağıdaki komut ile locust arayüzü oluşturulur. http://localhost:8089/ adresinden arayüze erişilir ve test başlatılr.
```
python -m locust -f load_test/load_test.py --users 1 --spawn-rate 1 --host https://www.cicek.com
```
Apache Jmeter için load_test.jmx dosyası kullanılabilir.

### Senaryo 3:
```
python -m pytest -vv -m installment
```