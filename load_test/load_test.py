# -*- encoding: utf-8 -*-

from locust import HttpUser, task, between


class QuickstartUser(HttpUser):
    wait_time = between(1, 5)

    @task
    def search_lilyum(self):
        self.client.get("/Catalog/AjaxCategory", params={'query': 'lilyum'})
