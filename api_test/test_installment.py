# -*- encoding: utf-8 -*-

import requests
from pytest import mark

URL = r'https://4ccbaa7a-8fd6-4724-880e-07ec7149a98e.mock.pstmn.io/test/installment={}'


def make_installment_request(endpoint):
    response = requests.get(URL.format(endpoint))
    status_code = response.status_code
    if status_code == 200:
        return response.json()['result']['data']['products']
    else:
        return status_code


def get_products_wo_installment():
    for product in make_installment_request('0'):
        yield product


def get_products_with_installment():
    for product in make_installment_request('1'):
        yield product


def idfn(arg):
    if isinstance(arg, dict):
        return arg['name']


@mark.installment
@mark.parametrize('product', get_products_wo_installment(), ids=idfn)
def check_without_installment_product_is(product):
    actual_result = {'productGroupId': product['productGroupId'], 'installmentText': product['installmentText'], 'installment': product['installment']}
    assert actual_result == {'productGroupId': 2, 'installmentText': '', 'installment': False}


@mark.installment
@mark.parametrize('product', get_products_with_installment(), ids=idfn)
def check_with_installment_product_is(product):
    actual_result = {'productGroupId': product['productGroupId'], 'installment': product['installment']}
    assert actual_result == {'productGroupId': 1, 'installment': True}
    assert product['installmentText'].endswith('Taksit Seçeneği')


@mark.installment
def check_null_installment():
    assert make_installment_request('') == 500
